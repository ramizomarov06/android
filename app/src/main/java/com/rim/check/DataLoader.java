package com.rim.check;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.logging.Handler;

public class DataLoader {
    private DatabaseReference databaseReference;
    private FirebaseAuth mAuth;
    private String userUid;
    private LinearLayout dataLayout;
    private Context context;
    private ServerStatusChecker serverStatusChecker;
    private DataLoaderCallback callback;

    public DataLoader(LinearLayout dataLayout, Context context,DataLoaderCallback callback) {
        this.dataLayout = dataLayout;
        this.context = context;
        this.callback = callback;
        mAuth = FirebaseAuth.getInstance();
        userUid = mAuth.getCurrentUser().getUid();
        serverStatusChecker = new ServerStatusChecker(dataLayout, context);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("servers");

    }
    public interface DataLoaderCallback {
        void onDataLoaded(DataSnapshot dataSnapshot);
    }

    public void loadDataFromDatabase() {
        databaseReference.orderByChild("userUid").equalTo(userUid).addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                dataLayout.removeAllViews(); // Clear the existing data before loading new data

                LayoutInflater inflater = LayoutInflater.from(context);

                for (DataSnapshot serverSnapshot : dataSnapshot.getChildren()) {
                    String title = serverSnapshot.child("title").getValue(String.class);
                    String method = serverSnapshot.child("method").getValue(String.class);
                    String url = serverSnapshot.child("url").getValue(String.class);

                    // Inflate the server_item_layout.xml
                    LinearLayout dataContainer = (LinearLayout) inflater.inflate(R.layout.server_item_layout, dataLayout, false);

                    // Find views from the inflated layout
                    TextView titleTextView = dataContainer.findViewById(R.id.titleTextView);
                    TextView methodTextView = dataContainer.findViewById(R.id.methodTextView);
//                    ImageView deleteButton = dataContainer.findViewById(R.id.deleteButton);
                    ImageView statusButton = dataContainer.findViewById(R.id.serverStatus);
                    Glide.with(context).
                            asGif().
                            load(R.drawable.server_offline)
                            .into(statusButton);

                    // Set the data to the views
                    titleTextView.setText(title);
                    methodTextView.setText(method);

//                    // Set an onClickListener for the delete button
//                    deleteButton.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            // Delete the server data from the database
//                            String serverKey = serverSnapshot.getKey();
//                            DatabaseReference serverRef = databaseReference.child(serverKey);
//                            serverRef.removeValue();
//                        }
//                    });

                    // Add the LinearLayout to the main dataLayout
                    dataLayout.addView(dataContainer);
                }
                // Start or stop the periodic server status check based on the number of servers
                int serverCount = (int) dataSnapshot.getChildrenCount();
                if (serverCount > 0 && !serverStatusChecker.isChecking()) {
                    serverStatusChecker.startServerStatusCheck();
                } else if (serverCount == 0 && serverStatusChecker.isChecking()) {
                    serverStatusChecker.stopServerStatusCheck();
                }
                if (callback != null) {
                    callback.onDataLoaded(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle any errors that occur during data retrieval
                // For simplicity, you can display an error message here.
            }
        });
    }
}
