package com.rim.check;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ServerStatusChecker {
    final Handler handler = new Handler(Looper.getMainLooper());
    private boolean isChecking = false;
    final DatabaseReference databaseReference;
    final FirebaseAuth mAuth;

    final LinearLayout dataLayout;
    final Context context;

    public ServerStatusChecker(LinearLayout dataLayout, Context context) {
        this.dataLayout = dataLayout;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("servers");
    }

    public boolean isChecking() {
        return isChecking;
    }

    public void startServerStatusCheck() {
        isChecking = true;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkServerStatus();
                handler.postDelayed(this, 10000); // Repeat every 30 seconds
            }
        }, 0);
    }

    public void stopServerStatusCheck() {
        isChecking = false;
        handler.removeCallbacksAndMessages(null);
        Log.d("Tag", "Request body" + UserLoginData.getUserEmail());
    }

    private void checkServerStatus() {

        databaseReference.orderByChild("userUid").equalTo(UserLoginData.getUserUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot serverSnapshot : dataSnapshot.getChildren()) {
                    String title = serverSnapshot.child("title").getValue(String.class);
                    String url = serverSnapshot.child("url").getValue(String.class);
                    updateServerSwitch(serverSnapshot.getKey(), title, url);


                    // Perform server status check
                    new Thread(() -> {
                        JSONObject jsonResponse = getServerResponse(url); // Получаем JSON-ответ
                        // Update the UI on the main thread
                        handler.post(() -> updateServerStatus(title, jsonResponse));
                    }).start();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle any errors that occur during data retrieval
                // For simplicity, you can display an error message here.
            }
        });

    }

    // Добавим новый метод updateServerSwitch()
    private void updateServerSwitch(String serverKey, String serverTitle, String serverUrl) {
        for (int i = 0; i < dataLayout.getChildCount(); i++) {
            View dataContainer = dataLayout.getChildAt(i);
            if (dataContainer instanceof LinearLayout) {
                TextView titleTextView = dataContainer.findViewById(R.id.titleTextView);
                ImageView statusButton = dataContainer.findViewById(R.id.serverStatus);
                @SuppressLint("UseSwitchCompatOrMaterialCode") Switch switchServer = dataContainer.findViewById(R.id.switchServer);

                if (titleTextView != null && titleTextView.getText() != null && titleTextView.getText().toString().equals(serverTitle)) {
                    switchServer.setOnCheckedChangeListener((buttonView, isChecked) -> {
                        // Отправляем POST-запрос, когда пользователь переключает Switch
                        sendPostRequestToServer(serverUrl, isChecked);
                        Glide.with(context)
                                .load(R.drawable.server_pending)
                                .into(statusButton);
                    });

                    // Запомним ключ сервера (serverKey) в теге переключателя Switch, чтобы мы могли идентифицировать его при изменении
                    switchServer.setTag(serverKey);
                }
            }
        }
    }

    private void sendPostRequestToServer(String urlStr, boolean isChecked) {
        try {
            URL url = new URL(urlStr);
            OkHttpClient client = new OkHttpClient();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", isChecked);

            RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json"));
            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    e.printStackTrace();
                    Log.e("ServerStatusChecker", "Error sending POST request: " + e.getMessage());
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) {
                    if (!response.isSuccessful()) {
                        Log.e("ServerStatusChecker", "Server responded with error code: " + response.code());
                    }
                    response.close();
                }
            });
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            Log.e("ServerStatusChecker", "Error creating or sending POST request: " + e.getMessage());
        }
    }

    private JSONObject getServerResponse(String urlStr) {
        try {
            URL url = new URL(urlStr);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(20000); // Set read timeout to 10 seconds (adjust as needed)
            connection.setConnectTimeout(25000); // Set connection timeout to 15 seconds (adjust as needed)
            connection.setRequestMethod("GET");
            connection.connect();

            // Проверяем код ответа сервера, чтобы убедиться, что запрос успешен
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                // Если ответ сервера не успешный, выводим лог и возвращаем null
                Log.e("ServerStatusChecker", "Server responded with error code: " + responseCode);
                return null;
            }

            // Считываем ответ в виде строки
            InputStream inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
            inputStream.close();

            // Преобразуем JSON-строку в JSON-объект
            return new JSONObject(response.toString());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            Log.e("ServerStatusChecker", "Error reading server response: " + e.getMessage());
            return null; // Возвращаем null в случае возникновения ошибки чтения ответа или парсинга JSON
        }
    }


    private void updateServerStatus(String serverTitle, JSONObject jsonResponse) {
        if (jsonResponse == null) {
            // Если JSON-ответ null, считаем, что сервер не отвечает
            for (int i = 0; i < dataLayout.getChildCount(); i++) {
                View dataContainer = dataLayout.getChildAt(i);
                if (dataContainer instanceof LinearLayout) {
                    TextView titleTextView = dataContainer.findViewById(R.id.titleTextView);
                    ImageView statusButton = dataContainer.findViewById(R.id.serverStatus);

                    if (titleTextView != null && titleTextView.getText() != null && titleTextView.getText().toString().equals(serverTitle)) {
                        Glide.with(context)
                                .load(R.drawable.server_pending)
                                .into(statusButton);
                        break;
                    }
                }
            }
            return; // Выходим из метода, чтобы избежать обработки null значения
        }

        try {
            if (jsonResponse.has("status")) {
                int status = jsonResponse.getInt("status");

                for (int i = 0; i < dataLayout.getChildCount(); i++) {
                    View dataContainer = dataLayout.getChildAt(i);
                    if (dataContainer instanceof LinearLayout) {
                        TextView titleTextView = dataContainer.findViewById(R.id.titleTextView);
                        ImageView statusButton = dataContainer.findViewById(R.id.serverStatus);
                        @SuppressLint("UseSwitchCompatOrMaterialCode") Switch switchServer = dataContainer.findViewById(R.id.switchServer);

                        if (titleTextView != null && titleTextView.getText() != null && titleTextView.getText().toString().equals(serverTitle)) {
                            if (status == 1) {
                                // Сервер работает
                                Glide.with(context)
                                        .load(R.drawable.server_online)
                                        .into(statusButton);
                                switchServer.setChecked(true);
                            } else {
                                // Сервер выключен или другое некорректное значение status
                                Glide.with(context)
                                        .load(R.drawable.server_offline)
                                        .into(statusButton);
                                switchServer.setChecked(false);
                            }
                            break;
                        }
                    }
                }
            } else {
                // Если поле "status" отсутствует в JSON-ответе, считаем, что сервер не отвечает
                for (int i = 0; i < dataLayout.getChildCount(); i++) {
                    View dataContainer = dataLayout.getChildAt(i);
                    if (dataContainer instanceof LinearLayout) {
                        TextView titleTextView = dataContainer.findViewById(R.id.titleTextView);
                        ImageView statusButton = dataContainer.findViewById(R.id.serverStatus);
                        @SuppressLint("UseSwitchCompatOrMaterialCode") Switch switchServer = dataContainer.findViewById(R.id.switchServer);

                        if (titleTextView != null && titleTextView.getText() != null && titleTextView.getText().toString().equals(serverTitle)) {
                            Glide.with(context)
                                    .load(R.drawable.server_pending)
                                    .into(statusButton);
                            switchServer.setFocusable(false);
                            switchServer.setClickable(false);
                            break;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            // Обработка ошибки парсинга JSON-ответа (если что-то пошло не так с парсингом)
            e.printStackTrace();
            // Если возникла ошибка парсинга, то также считаем, что сервер не отвечает
            for (int i = 0; i < dataLayout.getChildCount(); i++) {
                View dataContainer = dataLayout.getChildAt(i);
                if (dataContainer instanceof LinearLayout) {
                    TextView titleTextView = dataContainer.findViewById(R.id.titleTextView);
                    ImageView statusButton = dataContainer.findViewById(R.id.serverStatus);
                    @SuppressLint("UseSwitchCompatOrMaterialCode") Switch switchServer = dataContainer.findViewById(R.id.switchServer);

                    if (titleTextView != null && titleTextView.getText() != null && titleTextView.getText().toString().equals(serverTitle)) {
                        Glide.with(context)
                                .load(R.drawable.server_pending)
                                .into(statusButton);
                        switchServer.setFocusable(false);
                        switchServer.setClickable(false);
                        break;
                    }
                }
            }
        }
    }






}
