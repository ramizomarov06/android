package com.rim.check;



import com.getbase.floatingactionbutton.FloatingActionButton;


import android.app.Activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;


public class TestPage extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testpage);


        final FloatingActionButton actionA = findViewById(R.id.action_a);
        final FloatingActionButton actionB = findViewById(R.id.action_b);
        actionA.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                actionA.setTitle("Action A clicked");
            }
        });

        actionB.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                actionB.setTitle("Action B clicked");
            }
        });



    }
}