package com.rim.check;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.auth.FirebaseUser;


public class ServerAddAcitivity  extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.serveradd_layout);

        ImageView gifImageView = findViewById(R.id.cloudImageView);

        Glide.with(this)
                .asGif()
                .load(R.drawable.gif8)
                .into(gifImageView);


        Button positiveBtn = findViewById(R.id.positiveBtn);
        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (UserLoginData.isActive()) {
                    addServerToDatabase(UserLoginData.getUserUid());
                } else {
                    // Handle the case when the user is not logged in at all (shouldn't happen)
                    setResult(RESULT_CANCELED);
                    Toast.makeText(ServerAddAcitivity.this, "Вы не авторизованы", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
        Button negativeBtn = findViewById(R.id.negativeBtn);
        negativeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Нажатие на кнопку "Cancel" - закрываем текущую активность
                finish();
            }
        });
    }
    private void addServerToDatabase(String userUid) {
        String title = ((TextView) findViewById(R.id.title)).getText().toString();
        // String method = ((Spinner) findViewById(R.id.methodSpinner)).getSelectedItem().toString();
        String url = ((EditText) findViewById(R.id.urlEditText)).getText().toString();
        String json = ((EditText) findViewById(R.id.JsonInsert)).getText().toString();

        // Создаем ссылку на базу данных и устанавливаем путь к данным (например, "servers")
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("servers");

        // Создаем новую запись в базе данных с уникальным ключом (push) и устанавливаем значения
        DatabaseReference newServerRef = databaseReference.push();
        newServerRef.child("userUid").setValue(userUid); // Сохраняем UID пользователя
        newServerRef.child("title").setValue(title);
        // newServerRef.child("method").setValue(method);
        newServerRef.child("url").setValue(url);
        newServerRef.child("json").setValue(json);

        // Возвращаем результат и закрываем текущую активность
        setResult(RESULT_OK);
        finish();
    }
}
