package com.rim.check;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Скрыть системные навигационные кнопки и перейти в полноэкранный режим
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        setContentView(R.layout.activity_main);

        // Найти кнопку по идентификатору
        Button button = findViewById(R.id.Join);
        // Получение экземпляра FirebaseAuth
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        // Инициализируйте FirebaseAuth
        mAuth = FirebaseAuth.getInstance();

        // Получите текущего пользователя
        FirebaseUser user = mAuth.getCurrentUser();

        // Проверьте, что пользователь не равен null, а затем установите его UID в TextView
        SharedPreferences sharedPreferences = getSharedPreferences("MyPreferences", MODE_PRIVATE);
        String userUid = sharedPreferences.getString("userUid", null);
        String userEmail = sharedPreferences.getString("userEmail", null);
        String displayName = sharedPreferences.getString("displayName", null);
        String photoUrlString = sharedPreferences.getString("photoUrl", null);
        Uri photoUri = null;
        if (photoUrlString != null) {
            photoUri = Uri.parse(photoUrlString);
        }
        if (userUid != null) {
            UserLoginData.setUserUid(userUid);
            UserLoginData.setUserEmail(userEmail);
            UserLoginData.setGetDisplayName(displayName);
            UserLoginData.setGetPhotoUrl(photoUri);
            startActivity(new Intent(MainActivity.this, ProcessActivity.class));
            finish();
        }else{
            Toast.makeText(this, "Please log in .", Toast.LENGTH_SHORT).show();
        }

        // Установить обработчик нажатия на кнопку
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
