package com.rim.check;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import java.util.Map;

public class SeleniumCardClickListener implements View.OnClickListener {
    private LinearLayout dataLayout;
//    private PieChart pieChart;
    private boolean isSeleniumCardClicked = false;

    public SeleniumCardClickListener(LinearLayout dataLayout, PieChart pieChart) {
        this.dataLayout = dataLayout;
//        this.pieChart = pieChart;
    }

    @Override
    public void onClick(View v) {
        // Clear the existing views in dataLayout
        dataLayout.removeAllViews();


        isSeleniumCardClicked = true; // Set the flag to true
        loadDataFromDbTestresult();
    }

    private void loadDataFromDbTestresult() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("test_results");
        databaseReference.orderByChild("userEmail").equalTo(UserLoginData.getUserEmail()).addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot serverSnapshot : dataSnapshot.getChildren()) {
                        Map<String, Object> testData = (Map<String, Object>) serverSnapshot.getValue();
                        if (testData != null) {
                            Object successCountObj = testData.get("successCount");
                            Object skippedCountObj = testData.get("skippedCount");
                            Object failedCountObj = testData.get("failedCount");
                            Object linkObj = testData.get("link");

                            // Check if all required fields exist and are not null
                            if (successCountObj != null && skippedCountObj != null && failedCountObj != null && linkObj != null) {
                                int successCount = Integer.parseInt(successCountObj.toString());
                                int skippedCount = Integer.parseInt(skippedCountObj.toString());
                                int failedCount = Integer.parseInt(failedCountObj.toString());
                                String link = linkObj.toString();

                                System.out.println("Success Count: " + successCount);
                                System.out.println("Skipped Count: " + skippedCount);
                                System.out.println("Failed Count: " + failedCount);
                                System.out.println("Link: " + link);

                                // Inflate and add the selenium_layout to dataLayout
                                LayoutInflater inflater = LayoutInflater.from(dataLayout.getContext());
                                View seleniumLayout = inflater.inflate(R.layout.selenium_layout, dataLayout, false);
                                dataLayout.addView(seleniumLayout);

                                TextView skipped = seleniumLayout.findViewById(R.id.skippedCount);
                                TextView passed = seleniumLayout.findViewById(R.id.successCount);
                                TextView failed = seleniumLayout.findViewById(R.id.failedCount);
                                TextView linkTextView = seleniumLayout.findViewById(R.id.allureLink);
                                PieChart pieChart = seleniumLayout.findViewById(R.id.piechart);

                                // Set the data to the views
                                passed.setText(Integer.toString(successCount));
                                skipped.setText(Integer.toString(skippedCount));
                                failed.setText(Integer.toString(failedCount));
                                linkTextView.setText(link);


                                if (pieChart != null) {
                                    // Proceed with using the pieChart object
                                    // Set the data and color to the pie chart
                                    pieChart.clearChart();
                                    pieChart.addPieSlice(new PieModel("Passed", successCount, Color.parseColor("#66BB6A")));
                                    pieChart.addPieSlice(new PieModel("Skipped", skippedCount, Color.parseColor("#bcbcbc")));
                                    pieChart.addPieSlice(new PieModel("Failed", failedCount, Color.parseColor("#EF5350")));

                                    // To animate the pie chart
                                    pieChart.startAnimation();
                                } else {
                                    // Print a message or handle the case when the pieChart is null
                                    System.out.println("PieChart is null. Make sure it is correctly initialized in the layout.");
                                }

                                // ... Rest of the code for inflating views and updating UI


                            } else {
                                // Show no data message if any of the required fields is missing or null
                                showNoDataMessage();
                            }
                        }
                    }
                }else{
                    showNoDataMessage();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle any errors that occur during data retrieval
                // For simplicity, you can display an error message here.
            }
        });
    }
    private void showNoDataMessage() {
        // Inflate a layout to display the message
        LayoutInflater inflater = LayoutInflater.from(dataLayout.getContext());
        View noDataLayout = inflater.inflate(R.layout.no_data_layout, dataLayout, false);
        dataLayout.addView(noDataLayout);
        // Load the GIF as a drawable with Glide


        // Find the TextView to show the message
        TextView messageTextView = noDataLayout.findViewById(R.id.messageTextView);

        // Set the message to be displayed
        messageTextView.setText("У вас нет данных для отображения");

        // Optionally, you can also add an ImageView or any other UI elements to improve the message presentation.
    }
}


