package com.rim.check;

import android.net.Uri;

import java.util.Objects;

public class UserLoginData {
    static String userUid;
    static String userEmail;
    static String getDisplayName;
    static Uri getPhotoUrl;

    public static Uri getGetPhotoUrl() {
        return getPhotoUrl;
    }

    public static void setGetPhotoUrl(Uri getPhotoUrl) {
        UserLoginData.getPhotoUrl = getPhotoUrl;
    }



    public static String getGetDisplayName() {
        return getDisplayName;
    }

    public static void setGetDisplayName(String getDisplayName) {
        UserLoginData.getDisplayName = getDisplayName;
    }



    public static String getUserEmail() {
        return userEmail;
    }

    public static void setUserEmail(String userEmail) {
        UserLoginData.userEmail = userEmail;
    }

    public static String getUserUid() {
        return userUid;
    }

    public static void setUserUid(String userUid) {
        UserLoginData.userUid = userUid;
    }

    public static boolean isActive(){
        return Objects.nonNull(userUid);
    }
}
