package com.rim.check;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.Glide;

public class GlideRequestManagerFragment extends Fragment {
    @Override
    public void onDestroy() {
        super.onDestroy();
        // Clear Glide's request when the fragment is destroyed
        Glide.with(this).onDestroy();
    }
}

