package com.rim.check;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.cardview.widget.CardView;

import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import org.eazegraph.lib.charts.PieChart;




public class ProcessActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
//    private final Handler handler = new Handler(Looper.getMainLooper());
    private final boolean isChecking = false;
    TextView skipped,failed,passed,link;
    PieChart pieChart;
    private ServerStatusChecker serverStatusChecker;
    LinearLayout dataLayout;



    @SuppressLint({"SetTextI18n", "MissingInflatedId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process);
        skipped = findViewById(R.id.skippedCount);
        failed = findViewById(R.id.failedCount);
        passed = findViewById(R.id.successCount);
        pieChart = findViewById(R.id.piechart);
        link = findViewById(R.id.allureLink);
        final LinearLayout dataLayout = findViewById(R.id.dataLayout);
        final CardView AllureReport = findViewById(R.id.AllureCard);
        final FloatingActionButton actionRequest = findViewById(R.id.action_request_add);
        final FloatingActionButton actionServer = findViewById(R.id.action_server_add);
        actionRequest.setOnClickListener(view -> {
            Intent intent = new Intent(ProcessActivity.this, RequestAddActivity.class);
            serverAddLauncher.launch(intent);
        });
        actionServer.setOnClickListener(view -> {
            Intent intent = new Intent(ProcessActivity.this, ServerAddAcitivity.class);
            serverAddLauncher.launch(intent);
        });


        AllureReport.setOnClickListener(v ->{
            if (UserLoginData.isActive()) {
                new SeleniumCardClickListener(dataLayout,pieChart).onClick(v);
            }else {
                showAccessDeniedLayer();
            }
        });
        SharedPreferences sharedPreferences = getSharedPreferences("MyPreferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("userUid", UserLoginData.getUserUid());
        editor.putString("userEmail", UserLoginData.getUserEmail());
        editor.putString("displayName", UserLoginData.getGetDisplayName());
        if (UserLoginData.getGetPhotoUrl() != null) {
            String photoUrlString = UserLoginData.getGetPhotoUrl().toString();
            editor.putString("photoUrl", photoUrlString);
        } else {
            editor.remove("photoUrl"); // Если Uri getPhotoUrl равен null, удалите его из SharedPreferences
        }
        editor.apply();



        CardView onBoardCard = findViewById(R.id.botCard);
        onBoardCard.setOnClickListener(v -> {
            onBotCardClick(); // Вызываем обработчик нажатия на кнопку botCard
        });
        CardView RestAssured = findViewById(R.id.RestAssured);
        RestAssured.setOnClickListener(v -> onRestAssuredClick());

        // Инициализируйте FirebaseAuth
        mAuth = FirebaseAuth.getInstance();
        TextView uidTextView = findViewById(R.id.userName); //Login name(email)

        // Проверьте, что пользователь не равен null, а затем установите его UID в TextView
        if (UserLoginData.isActive()) {
            uidTextView.setText(UserLoginData.getGetDisplayName());



//            loadDataFromDatabase();
           serverStatusChecker = new ServerStatusChecker(dataLayout, this);
        } else {
            uidTextView.setText("Guest");
            showAccessDeniedLayer();
        }

        // Find the logout ImageView by its ID and set an OnClickListener
        ImageView logoutImageView = findViewById(R.id.Logout);
        logoutImageView.setOnClickListener(v -> logout());
        ImageView DisplayPhoto = findViewById(R.id.corpIcon);

        if (UserLoginData.isActive()) {
            if (UserLoginData.getGetPhotoUrl() != null) {

                // If the user is logged in and has a profile photo URL, load the photo using Glide
                Glide.with(this)
                        .load(UserLoginData.getGetPhotoUrl())
                        .apply(RequestOptions.circleCropTransform()) // To display the image in circular shape
                        .placeholder(R.drawable.refresh) // Placeholder image while loading
                        .error(R.drawable.ic_small) // Image to show in case of error or no profile photo
                        .into(DisplayPhoto);

            } else {
                Glide.with(this)
                        .load(R.drawable.guest_small)
                        .apply(RequestOptions.circleCropTransform()) // To display the image in circular shape
                        .placeholder(R.drawable.refresh) // Placeholder image while loading
                        .error(R.drawable.guest_small) // Image to show in case of error
                        .into(new DrawableImageViewTarget(DisplayPhoto) {
                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                super.onResourceReady(resource, transition);
                                // Start the animation once the GIF drawable is loaded
                                if (resource instanceof GifDrawable) {
                                    ((GifDrawable) resource).setLoopCount(1); // Play the GIF only once
                                    ((GifDrawable) resource).start();
                                }
                            }
                        });

            }
        } else {
            // If the user is a guest, load the "guest_small" image directly
            Glide.with(this)
                    .load(R.drawable.guest_small)
                    .apply(RequestOptions.circleCropTransform()) // To display the image in circular shape
                    .placeholder(R.drawable.refresh) // Placeholder image while loading
                    .error(R.drawable.guest_small) // Image to show in case of error
                    .into(new DrawableImageViewTarget(DisplayPhoto) {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            super.onResourceReady(resource, transition);
                            // Start the animation once the GIF drawable is loaded
                            if (resource instanceof GifDrawable) {
                                ((GifDrawable) resource).setLoopCount(1); // Play the GIF only once
                                ((GifDrawable) resource).start();
                            }
                        }
                    });
        }

    }



    private void loadDataFromDatabase() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("servers");
        databaseReference.orderByChild("userUid").equalTo(UserLoginData.getUserUid()).addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                LinearLayout dataLayout = findViewById(R.id.dataLayout);
                dataLayout.removeAllViews(); // Clear the existing data before loading new data

                LayoutInflater inflater = LayoutInflater.from(ProcessActivity.this);

                for (DataSnapshot serverSnapshot : dataSnapshot.getChildren()) {
                    String title = serverSnapshot.child("title").getValue(String.class);
                    String url = serverSnapshot.child("url").getValue(String.class);


                    // Inflate the server_item_layout.xml
                    LinearLayout dataContainer = (LinearLayout) inflater.inflate(R.layout.server_item_layout, dataLayout, false);

                    // Find views from the inflated layout
                    TextView titleTextView = dataContainer.findViewById(R.id.titleTextView);
                    TextView methodTextView = dataContainer.findViewById(R.id.methodTextView);
                    ImageView statusButton = dataContainer.findViewById(R.id.serverStatus);
                    Glide.with(ProcessActivity.this).
                            asGif().
                            load(R.drawable.server_offline)
                            .into(statusButton);


                    // Set the data to the views
                    titleTextView.setText(title);
                    methodTextView.setText(url);

                    // Add the LinearLayout to the main dataLayout
                    dataLayout.addView(dataContainer);
                }
                // Start or stop the periodic server status check based on the number of servers
                int serverCount =(int) dataSnapshot.getChildrenCount();
                if (serverCount > 0 && !isChecking) {
                    serverStatusChecker.startServerStatusCheck();
                } else if (serverCount == 0 && isChecking) {
                    serverStatusChecker.stopServerStatusCheck();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle any errors that occur during data retrieval
                // For simplicity, you can display an error message here.
            }
        });
    }


    private final ActivityResultLauncher<Intent> serverAddLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    // Reload the data from the database after a new server is added
                    loadDataFromDatabase();
                }
            }
    );

    // Function to handle logout
    private void logout() {
        if(UserLoginData.isActive()){
            Log.d("ProcessActivity", "Logout method called.");
            showLogoutConfirmationPopup();
        }else{
            Intent intent = new Intent(ProcessActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }


    }

    private void showLogoutConfirmationPopup() {
        Log.d("ProcessActivity", "Logout confirmation popup shown.");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.logout_confirmation_layout, null);
        ImageView imageView = dialogView.findViewById(R.id.ExitLogoutIcon);
        Glide.with(this)
                .load(R.drawable.warning_notify)
                .into(new DrawableImageViewTarget(imageView) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        // Start the animation once the GIF drawable is loaded
                        if (resource instanceof GifDrawable) {
                            ((GifDrawable) resource).setLoopCount(1); // Play the GIF only once
                            ((GifDrawable) resource).start();
                        }
                    }
                });

        builder.setView(dialogView)
                .setPositiveButton("Yes", (dialog, which) -> {
                    if(UserLoginData.isActive()){
                        Log.d("ProcessActivity", "User confirmed logout.");
                        // User confirmed logout, proceed with the logout process
                        mAuth.signOut();
                        SharedPreferences sharedPreferences = getSharedPreferences("MyPreferences", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.remove("userUid");
                        editor.remove("photoUrl");
                        editor.remove("userEmail");
                        editor.apply();
                        UserLoginData.setUserUid(null);
                        UserLoginData.setGetPhotoUrl(null);
                        UserLoginData.setUserEmail(null);
                        serverStatusChecker.stopServerStatusCheck();
                        Intent intent = new Intent(ProcessActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
//                    finish();
                    }else{
                        Intent intent = new Intent(ProcessActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }

                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    // Метод, который будет вызываться при нажатии на кнопку botCard
    public void onBotCardClick() {

        if (UserLoginData.isActive()) {

            loadDataFromDatabase(); // Вызываем метод для загрузки данных из базы данных

        } else {
            LinearLayout dataLayout = findViewById(R.id.dataLayout);
            dataLayout.removeAllViews();
            showAccessDeniedLayer();
            // Handle the case when the user is not logged in
            // For example, you can display a message or redirect the user to the login screen.
            // In this case, I will show a Toast message for simplicity.
            Toast.makeText(this, "Please log in to access this feature.", Toast.LENGTH_SHORT).show();
        }
        // Initialize the serverStatusChecker if it's null
        if (serverStatusChecker == null) {
            serverStatusChecker = new ServerStatusChecker(dataLayout, this);
        }

    }

    public void  onRestAssuredClick(){
        if (UserLoginData.isActive()){
            Toast.makeText(this, "Coming...", Toast.LENGTH_SHORT).show();
        }
        else{
            LinearLayout dataLayout = findViewById(R.id.dataLayout);
            dataLayout.removeAllViews();
            showAccessDeniedLayer();
            // Handle the case when the user is not logged in
            // For example, you can display a message or redirect the user to the login screen.
            // In this case, I will show a Toast message for simplicity.
            Toast.makeText(this, "Please log in to access this feature.", Toast.LENGTH_SHORT).show();
        }
    }
    private void showAccessDeniedLayer() {
        LinearLayout dataLayout = findViewById(R.id.dataLayout);
        dataLayout.removeAllViews();

        // Inflate the access_denied_layer.xml
        View accessDeniedLayer = getLayoutInflater().inflate(R.layout.access_denided, dataLayout, false);
        ImageView imageView = accessDeniedLayer.findViewById(R.id.accessDeniedImage);

        // Load the GIF as a drawable with Glide
        Glide.with(this)
                .load(R.drawable.access_denied_ghost)
                .into(new DrawableImageViewTarget(imageView) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        // Start the animation once the GIF drawable is loaded
                        if (resource instanceof GifDrawable) {
                            ((GifDrawable) resource).setLoopCount(1); // Play the GIF only once
                            ((GifDrawable) resource).start();
                        }
                    }
                });

        // Add the access_denied_layer to the main dataLayout
        dataLayout.addView(accessDeniedLayer);
        Toast.makeText(this, "Please log in to access all feature.", Toast.LENGTH_SHORT).show();
    }

    private void showExitConfirmationPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.exit_confirmation_layout, null);
        ImageView imageView = dialogView.findViewById(R.id.ExitPopUpImage);
        Glide.with(this)
                .load(R.drawable.info_image)
                .into(new DrawableImageViewTarget(imageView) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        // Start the animation once the GIF drawable is loaded
                        if (resource instanceof GifDrawable) {
                            ((GifDrawable) resource).setLoopCount(1); // Play the GIF only once
                            ((GifDrawable) resource).start();
                        }
                    }
                });

        // Set an image to the ImageView (if needed)
        // imageView.setImageResource(R.drawable.your_image_here);

        builder.setView(dialogView)
                .setPositiveButton("Yes", (dialog, which) -> finish())
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    @Override
    public void onBackPressed() {
        if(UserLoginData.isActive()){
            // Show the exit confirmation popup
            showExitConfirmationPopup();
        }else{
            Intent intent = new Intent(ProcessActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

    }



}
