package com.rim.check;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.AuthResult;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Collection;

public class LoginActivity extends AppCompatActivity {

    // Определите код для запроса входа через Google (может быть любым числом)
    private static final int RC_SIGN_IN = 123;
    // Определите экземпляр GoogleSignInClient
    private GoogleSignInClient mGoogleSignInClient;

    // Определите ActivityResultLauncher
    private ActivityResultLauncher<Intent> signInLauncher;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Скрыть системные навигационные кнопки и перейти в полноэкранный режим
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        setContentView(R.layout.activity_login);
        // Найти кнопку по идентификатору
        Button guestUpbutton = findViewById(R.id.Guest);
        // Найти кнопку "SignIn" по идентификатору
        Button signInButton = findViewById(R.id.SignIn);

        TextView forgotPassword = findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,TestPage.class);
                startActivity(intent);
            }
        });

        // Установить обработчик нажатия на кнопку
        guestUpbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ProcessActivity.class);
                startActivity(intent);
            }
        });

        // Получение экземпляра FirebaseAuth
        FirebaseAuth mAuth = FirebaseAuth.getInstance();


        // Найти EditText поля по идентификаторам
        EditText emailEditText = findViewById(R.id.emailEditText);
        EditText passwordEditText = findViewById(R.id.passwordEditText);

        // Установить обработчик нажатия на кнопку "SignIn"
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Получение данных из полей email и password
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                // Попытка аутентификации пользователя
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Аутентификация успешна, можно получить информацию о пользователе
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    if (user != null){
                                        Log.d("Tag", "User UID:" + user.getEmail());
//                                        String userUid = user.getEmail();
                                        UserLoginData.setUserUid(user.getUid());
                                        UserLoginData.setGetDisplayName(user.getEmail());

                                        // Теперь сохраните этот userUid в базу данных Firebase под узлом "users" (например)
                                        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child("users");
                                        usersRef.child(UserLoginData.getUserUid()).setValue(true);
                                    }

                                    // переход
                                    Intent intent = new Intent(LoginActivity.this, ProcessActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    // Аутентификация не удалась, обработайте ошибку
                                    Toast.makeText(LoginActivity.this, "Ошибка аутентификации.",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        // Создайте параметры входа через Google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Создайте GoogleSignInClient с помощью параметров входа
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // Найдите кнопку "GoogleAuth" по идентификатору
        ImageView googleAuth = findViewById(R.id.GoogleAuhth);
        // Создайте ActivityResultLauncher для обработки входа через Google
        signInLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                            handleSignInResult(task);

                        }
                    }
                }
        );

        // Установите обработчик нажатия на кнопку "GoogleAuth"
        googleAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Запустите окно выбора учетной записи Google для входа
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                signInLauncher.launch(signInIntent);
            }
        });
    }

    // Добавьте метод для обработки результата входа через Google
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Вход успешен, получите информацию об учетной записи Google
            if (account != null) {
                UserLoginData.setUserUid(account.getId());
                UserLoginData.setUserEmail(account.getEmail());
                UserLoginData.setGetDisplayName(account.getDisplayName());
                UserLoginData.setGetPhotoUrl(account.getPhotoUrl());
                String photoUrl = account.getPhotoUrl() != null ? account.getPhotoUrl().toString() : "";
                Toast.makeText(LoginActivity.this, "Добро пожаловать: "+UserLoginData.getGetDisplayName(), Toast.LENGTH_SHORT).show();
                Log.d("Tag", "User PhotoUrl:" + UserLoginData.getGetPhotoUrl());
                // Теперь сохраните этот userUid в базу данных Firebase под узлом "users" (например)
                DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child("users");
                usersRef.child(UserLoginData.getUserUid()).setValue(true);

                // Дополнительный код, который вы хотите выполнить после успешного входа

                // Переход на другую активити (если это нужно)
                Intent intent = new Intent(LoginActivity.this, ProcessActivity.class);
                startActivity(intent);
                finish();
            }
        } catch (ApiException e) {
            // Ошибка входа через Google, обработайте ошибку
            Toast.makeText(LoginActivity.this, "Ошибка входа через Google.", Toast.LENGTH_SHORT).show();
        }
    }

}
