package com.rim.check;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class RequestAddActivity extends AppCompatActivity {
    private LinearLayout parentLayout;
    private Spinner statusResponseSpinner;
    private EditText responseEditText;
    private TextView requestEditText;
    private EditText firstEditBase;
    private boolean isFirstEditFilled = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.requestadd_layout);
        ImageView gifImageView = findViewById(R.id.cloudImageView);
        firstEditBase = findViewById(R.id.firstEditBase);
        firstEditBase.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isFirstEditFilled = true;
                updateRequestEditText();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        requestEditText = findViewById(R.id.requestSetText);
        Glide.with(this)
                .asGif()
                .load(R.drawable.gif8)
                .into(gifImageView);
        Button positiveBtn = findViewById(R.id.positiveBtn);
        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (UserLoginData.isActive()){
                    Log.d("Tag", "Request body" + UserLoginData.getUserEmail());
                    String requestBody = requestEditText.getText().toString();
                    Log.d("Tag", "Request body: " + requestBody);
                    addRequestsToDatabase(UserLoginData.userUid);

                }else {
                    setResult(RESULT_CANCELED);
                    Toast.makeText(RequestAddActivity.this, "You are not authorized", Toast.LENGTH_SHORT).show();
                }
            }
        });
        Button negativeBtn = findViewById(R.id.negativeBtn);
        negativeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        // Find views for addBaseButton and parentLayout
        Button addBaseButton = findViewById(R.id.addBaseButton);
        parentLayout = findViewById(R.id.parentLayout);

        addBaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEditText();
            }
        });

        // Find views for statusResponseSpinner and responseEditText
        statusResponseSpinner = findViewById(R.id.statusResponseSpinner);
        responseEditText = findViewById(R.id.responseEditText);

        // Set up the statusResponseSpinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.status_response_types,
                android.R.layout.simple_spinner_item
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusResponseSpinner.setAdapter(adapter);

        // Set up the listener for statusResponseSpinner
        statusResponseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if ("Response".equals(selectedItem)) {
                    responseEditText.setVisibility(View.VISIBLE);
                } else {
                    responseEditText.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    private List<EditText> editTextList = new ArrayList<>();
    private void addEditText() {
        LinearLayout layout = new LinearLayout(this);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));

        EditText editText = new EditText(this);
        editText.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        ));
        editText.setHint("Enter something");
        editTextList.add(editText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateRequestEditText(); // Вызываем updateRequestEditText() при изменении текста
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ImageView deleteButton = new ImageButton(this);
        deleteButton.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT

        ));
        deleteButton.setImageResource(R.drawable.delete_icon_40);
        deleteButton.setBackgroundResource(android.R.color.transparent);// Замените delete на название своего изображения
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentLayout.removeView(layout); // Удаляем layout при нажатии кнопки "Удалить"
                editTextList.remove(editText); // Удаляем EditText из списка при удалении
                updateRequestEditText();
            }
        });

        layout.addView(editText);
        layout.addView(deleteButton);
        if (!isFirstEditFilled) {
            firstEditBase = editText; // Сохраняем первый созданный EditText
            isFirstEditFilled = true;
        }

        parentLayout.addView(layout, parentLayout.getChildCount() - 1);
        updateRequestEditText();
        if (firstEditBase == null) {
            firstEditBase = editText; // Сохраняем первый созданный EditText
            requestEditText.setText(firstEditBase.getText().toString()); // Обновляем requestEditText
        }
        if (editTextList.size() == 1) {
            requestEditText.setText(editText.getText().toString());
        }
    }

    private void updateRequestEditText() {
        StringBuilder stringBuilder = new StringBuilder();

        if (isFirstEditFilled) {
            stringBuilder.append(firstEditBase.getText().toString());
        }

        for (EditText editText : editTextList) {
            stringBuilder.append(editText.getText().toString()); // Добавляем текст из всех EditText в строку
        }

        requestEditText.setText(stringBuilder.toString());
    }




    private void addRequestsToDatabase(String userUid){
        String title = ((TextView) findViewById(R.id.titleRequest)).getText().toString();
        String request = ((Spinner) findViewById(R.id.requestTypeSpinner)).getSelectedItem().toString();
        String requestLink = ((TextView) findViewById(R.id.requestSetText)).getText().toString();

        // Создаем ссылку на базу данных и устанавливаем путь к данным (например, "servers")
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("urlrequest");

        // Создаем новую запись в базе данных с уникальным ключом (push) и устанавливаем значения
        DatabaseReference newServerRef = databaseReference.push();
        newServerRef.child("userUid").setValue(userUid); // Сохраняем UID пользователя
        newServerRef.child("title").setValue(title);
        // newServerRef.child("method").setValue(method);
        newServerRef.child("request").setValue(request);
        newServerRef.child("requestLink").setValue(requestLink);


        // Возвращаем результат и закрываем текущую активность
        setResult(RESULT_OK);
        finish();
    }






}
